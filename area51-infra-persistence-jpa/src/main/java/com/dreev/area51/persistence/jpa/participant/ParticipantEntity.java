package com.dreev.area51.persistence.jpa.participant;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "participant")
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
@Getter
@Setter(AccessLevel.PROTECTED)
@EqualsAndHashCode
public class ParticipantEntity {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

}
