package com.dreev.area51.persistence.jpa.participant;

import com.dreev.area51.domain.Participant;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class ParticipantEntityMapper {

    public Participant fromEntity(ParticipantEntity entity) {
        return Participant.builder()
                .id(UUID.fromString(entity.getId()))
                .name(entity.getName())
                .build();
    }

    public ParticipantEntity toEntity(Participant participant) {
        return ParticipantEntity.builder()
                .id(participant.getId().toString())
                .name(participant.getName())
                .build();
    }

}
