package com.dreev.area51.persistence.jpa.participant;

import com.dreev.area51.domain.Participant;
import com.dreev.area51.domain.ParticipantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ParticipantRepositoryJpaImpl implements ParticipantRepository {

    private final ParticipantEntityMapper participantEntityMapper;
    private final ParticipantJpaRepository participantJpaRepository;

    @Override
    public List<Participant> findAll() {
        return participantJpaRepository.findAll()
                .stream()
                .map(participantEntityMapper::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Participant> findById(UUID participantId) {
        return participantJpaRepository.findById(participantId.toString())
                .map(participantEntityMapper::fromEntity);
    }

    @Override
    public Participant save(Participant participant) {
        ParticipantEntity toBeSaved = participantEntityMapper.toEntity(participant);
        ParticipantEntity saved = participantJpaRepository.save(toBeSaved);
        return participantEntityMapper.fromEntity(saved);
    }

}
