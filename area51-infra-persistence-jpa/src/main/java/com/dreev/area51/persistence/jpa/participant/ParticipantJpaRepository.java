package com.dreev.area51.persistence.jpa.participant;

import org.springframework.data.jpa.repository.JpaRepository;

interface ParticipantJpaRepository extends JpaRepository<ParticipantEntity, String> {

}
