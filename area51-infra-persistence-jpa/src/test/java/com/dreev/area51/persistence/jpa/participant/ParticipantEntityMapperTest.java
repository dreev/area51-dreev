package com.dreev.area51.persistence.jpa.participant;

import com.dreev.area51.domain.Participant;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ParticipantEntityMapperTest {

    private static final UUID PARTICIPANT_ID = UUID.randomUUID();
    private static final String PARTICIPANT_NAME = "aParticipant";

    @InjectMocks
    private ParticipantEntityMapper participantEntityMapper;

    private Participant participant;
    private ParticipantEntity participantEntity;

    @Before
    public void before() {
        participant = Participant.builder()
                .id(PARTICIPANT_ID)
                .name(PARTICIPANT_NAME)
                .build();
        participantEntity = ParticipantEntity.builder()
                .id(PARTICIPANT_ID.toString())
                .name(PARTICIPANT_NAME)
                .build();
    }

    @Test
    public void when_fromEntity_then_ok() {
        Participant actual = participantEntityMapper.fromEntity(participantEntity);
        assertThat(actual).isEqualTo(participant);
    }

    @Test
    public void when_toEntity_then_ok() {
        ParticipantEntity actual = participantEntityMapper.toEntity(participant);
        assertThat(actual).isEqualTo(participantEntity);
    }

}
