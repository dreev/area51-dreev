package com.dreev.area51.persistence.jpa.participant;

import com.dreev.area51.domain.Participant;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ParticipantRepositoryJpaImplTest {

    private static final UUID PARTICIPANT_ID = UUID.randomUUID();
    private static final String PARTICIPANT_NAME = "aParticipant";

    @Mock
    private ParticipantEntityMapper participantEntityMapper;

    @Mock
    private ParticipantJpaRepository participantJpaRepository;

    @InjectMocks
    private ParticipantRepositoryJpaImpl participantRepositoryJpa;

    private Participant participant;

    @Before
    public void before() {
        participant = Participant.builder()
                .id(PARTICIPANT_ID)
                .name(PARTICIPANT_NAME)
                .build();

        ParticipantEntity participantEntity = ParticipantEntity.builder()
                .id(PARTICIPANT_ID.toString())
                .name(PARTICIPANT_NAME)
                .build();

        when(participantEntityMapper.fromEntity(eq(participantEntity)))
                .thenReturn(participant);
        when(participantEntityMapper.toEntity(participant))
                .thenReturn(participantEntity);

        when(participantJpaRepository.findAll())
                .thenReturn(Collections.singletonList(participantEntity));
        when(participantJpaRepository.findById(eq(PARTICIPANT_ID.toString())))
                .thenReturn(Optional.of(participantEntity));
        when(participantJpaRepository.save(eq(participantEntity)))
                .thenReturn(participantEntity);
    }

    @Test
    public void when_findAll_then_ok() {
        List<Participant> actual = participantRepositoryJpa.findAll();
        assertThat(actual).hasSize(1);

        Participant participant = actual.get(0);
        assertThat(participant).isEqualTo(this.participant);
    }

    @Test
    public void when_findById_then_ok() {
        Optional<Participant> actual = participantRepositoryJpa.findById(PARTICIPANT_ID);
        assertThat(actual).isPresent();
        assertThat(actual.get()).isEqualTo(this.participant);
    }

    @Test
    public void when_create_then_ok() {
        Participant actual = participantRepositoryJpa.save(participant);
        assertThat(actual).isEqualTo(participant);
    }

}
