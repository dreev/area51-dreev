package com.dreev.area51.messaging.rabbit.participant;

import com.dreev.area51.client.dto.ParticipantCreatedEventDto;
import com.dreev.area51.client.dto.ParticipantDto;
import com.dreev.area51.domain.Participant;
import com.dreev.area51.domain.ParticipantCreatedEvent;
import com.dreev.area51.dto.ParticipantEventDtoMapper;
import com.dreev.area51.messaging.rabbit.RabbitMqExchangesProperties;
import com.dreev.area51.messaging.rabbit.RabbitMqProperties;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.amqp.core.AmqpTemplate;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ParticipantMessageProducerRabbitImplTest {

    private static final String AREA51_EVENT_EXCHANGE = "area51-event-exchange";
    private static final UUID PARTICIPANT_ID = UUID.randomUUID();
    private static final String PARTICIPANT_NAME = "aParticipant";

    @Mock
    private AmqpTemplate amqpTemplate;

    @Mock
    private RabbitMqProperties rabbitMqProperties;

    @Mock
    private ParticipantEventDtoMapper participantEventDtoMapper;

    @InjectMocks
    private ParticipantMessageProducerRabbitImpl participantMessageProducer;

    private ParticipantCreatedEvent participantCreatedEvent;
    private ParticipantCreatedEventDto participantCreatedEventDto;

    @Before
    public void before() {
        Participant participant = Participant.builder()
                .id(PARTICIPANT_ID)
                .name(PARTICIPANT_NAME)
                .build();
        participantCreatedEvent = ParticipantCreatedEvent.builder()
                .participant(participant)
                .build();

        ParticipantDto participantDto = ParticipantDto.builder()
                .id(PARTICIPANT_ID.toString())
                .name(PARTICIPANT_NAME)
                .build();
        participantCreatedEventDto = ParticipantCreatedEventDto.builder()
                .participant(participantDto)
                .build();

        RabbitMqExchangesProperties rabbitMqExchangesProperties = new RabbitMqExchangesProperties();
        rabbitMqExchangesProperties.setArea51Event(AREA51_EVENT_EXCHANGE);

        when(rabbitMqProperties.getExchanges())
                .thenReturn(rabbitMqExchangesProperties);

        when(participantEventDtoMapper.toDto(eq(participantCreatedEvent)))
                .thenReturn(participantCreatedEventDto);
    }

    @Test
    public void when_send_then_ok() {
        participantMessageProducer.send(participantCreatedEvent);
        verify(amqpTemplate, times(1))
                .convertAndSend(eq(AREA51_EVENT_EXCHANGE), eq(""), eq(participantCreatedEventDto));
    }

}
