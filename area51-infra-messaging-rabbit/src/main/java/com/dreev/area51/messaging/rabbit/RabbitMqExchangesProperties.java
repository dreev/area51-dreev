package com.dreev.area51.messaging.rabbit;

import lombok.Data;

@Data
public class RabbitMqExchangesProperties {

    private String area51Event;

}
