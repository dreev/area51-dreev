package com.dreev.area51.messaging.rabbit;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("dreev.rabbitmq")
@Data
public class RabbitMqProperties {

    private RabbitMqExchangesProperties exchanges;

}
