package com.dreev.area51.messaging.rabbit.participant;

import com.dreev.area51.client.dto.ParticipantCreatedEventDto;
import com.dreev.area51.domain.ParticipantCreatedEvent;
import com.dreev.area51.domain.ParticipantMessageProducer;
import com.dreev.area51.dto.ParticipantEventDtoMapper;
import com.dreev.area51.messaging.rabbit.RabbitMqProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ParticipantMessageProducerRabbitImpl implements ParticipantMessageProducer {

    private final AmqpTemplate amqp;
    private final ParticipantEventDtoMapper participantEventDtoMapper;
    private final RabbitMqProperties rabbitMqProperties;

    @Override
    public void send(ParticipantCreatedEvent event) {
        String exchange = rabbitMqProperties.getExchanges().getArea51Event();
        ParticipantCreatedEventDto dto = participantEventDtoMapper.toDto(event);
        amqp.convertAndSend(exchange, "", dto);
    }

}
