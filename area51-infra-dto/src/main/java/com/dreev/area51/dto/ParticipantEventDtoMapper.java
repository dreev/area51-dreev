package com.dreev.area51.dto;

import com.dreev.area51.client.dto.ParticipantCreatedEventDto;
import com.dreev.area51.client.dto.ParticipantDto;
import com.dreev.area51.domain.ParticipantCreatedEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ParticipantEventDtoMapper {

    private final ParticipantDtoMapper participantDtoMapper;

    public ParticipantCreatedEventDto toDto(ParticipantCreatedEvent participantCreatedEvent) {
        ParticipantDto participant = participantDtoMapper.toDto(participantCreatedEvent.getParticipant());

        return ParticipantCreatedEventDto.builder()
                .participant(participant)
                .build();
    }

}
