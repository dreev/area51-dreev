package com.dreev.area51.dto;

import com.dreev.area51.client.dto.ParticipantCreationRequestDto;
import com.dreev.area51.client.dto.ParticipantDto;
import com.dreev.area51.client.dto.ParticipantsDto;
import com.dreev.area51.domain.Participant;
import com.dreev.area51.domain.ParticipantCreationRequest;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ParticipantDtoMapper {

    public ParticipantDto toDto(Participant participant) {
        return ParticipantDto.builder()
                .id(participant.getId().toString())
                .name(participant.getName())
                .build();
    }

    public ParticipantsDto toDto(List<Participant> participants) {
        List<ParticipantDto> participantDtos = participants.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
        return ParticipantsDto.builder()
                .participants(participantDtos)
                .build();
    }

    public ParticipantCreationRequest fromDto(ParticipantCreationRequestDto dto) {
        return ParticipantCreationRequest.builder()
                .name(dto.getName())
                .build();
    }

}
