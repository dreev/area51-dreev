package com.dreev.area51.dto;

import com.dreev.area51.client.dto.ParticipantCreationRequestDto;
import com.dreev.area51.client.dto.ParticipantDto;
import com.dreev.area51.client.dto.ParticipantsDto;
import com.dreev.area51.domain.Participant;
import com.dreev.area51.domain.ParticipantCreationRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ParticipantDtoMapperTest {

    private static final UUID PARTICIPANT_ID = UUID.randomUUID();
    private static final String PARTICIPANT_NAME = "aParticipant";

    @InjectMocks
    private ParticipantDtoMapper participantDtoMapper;

    private Participant participant;
    private ParticipantDto participantDto;
    private ParticipantCreationRequest participantCreationRequest;
    private ParticipantCreationRequestDto participantCreationRequestDto;

    @Before
    public void before() {
        participant = Participant.builder()
                .id(PARTICIPANT_ID)
                .name(PARTICIPANT_NAME)
                .build();
        participantDto = ParticipantDto.builder()
                .id(PARTICIPANT_ID.toString())
                .name(PARTICIPANT_NAME)
                .build();
        participantCreationRequest = ParticipantCreationRequest.builder()
                .name(PARTICIPANT_NAME)
                .build();
        participantCreationRequestDto = ParticipantCreationRequestDto.builder()
                .name(PARTICIPANT_NAME)
                .build();
    }

    @Test
    public void when_toDto_then_ok() {
        ParticipantDto actual = participantDtoMapper.toDto(participant);
        assertThat(actual).isEqualTo(participantDto);
    }

    @Test
    public void when_toDtos_then_ok() {
        ParticipantsDto actual = participantDtoMapper.toDto(Collections.singletonList(participant));

        List<ParticipantDto> participants = actual.getParticipants();
        assertThat(participants).hasSize(1);

        ParticipantDto participantDto = participants.get(0);
        assertThat(participantDto).isEqualTo(this.participantDto);
    }

    @Test
    public void when_fromDto_then_ok() {
        ParticipantCreationRequest actual = participantDtoMapper.fromDto(participantCreationRequestDto);
        assertThat(actual).isEqualTo(participantCreationRequest);
    }

}
