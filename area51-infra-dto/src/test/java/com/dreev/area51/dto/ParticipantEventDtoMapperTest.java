package com.dreev.area51.dto;

import com.dreev.area51.client.dto.ParticipantCreatedEventDto;
import com.dreev.area51.client.dto.ParticipantDto;
import com.dreev.area51.domain.Participant;
import com.dreev.area51.domain.ParticipantCreatedEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ParticipantEventDtoMapperTest {

    private static final UUID PARTICIPANT_ID = UUID.randomUUID();
    private static final String PARTICIPANT_NAME = "aParticipant";

    @Mock
    private ParticipantDtoMapper participantDtoMapper;

    @InjectMocks
    private ParticipantEventDtoMapper participantEventDtoMapper;

    private ParticipantCreatedEvent participantCreatedEvent;
    private ParticipantCreatedEventDto participantCreatedEventDto;

    @Before
    public void before() {
        Participant participant = Participant.builder()
                .id(PARTICIPANT_ID)
                .name(PARTICIPANT_NAME)
                .build();
        participantCreatedEvent = ParticipantCreatedEvent.builder()
                .participant(participant)
                .build();

        ParticipantDto participantDto = ParticipantDto.builder()
                .id(PARTICIPANT_ID.toString())
                .name(PARTICIPANT_NAME)
                .build();
        participantCreatedEventDto = ParticipantCreatedEventDto.builder()
                .participant(participantDto)
                .build();

        when(participantDtoMapper.toDto(participant))
                .thenReturn(participantDto);
    }

    @Test
    public void when_toDto_then_ok() {
        ParticipantCreatedEventDto actual = participantEventDtoMapper.toDto(participantCreatedEvent);
        assertThat(actual).isEqualTo(participantCreatedEventDto);
    }

}
