# AREA 51 package architecture explanations

This document mainly explains how the project package architecture should be done and also why we should do it this way.

## How to organise packages

As you probably already know, package organization is one of the most important element to take into account inside a project.
It gives readability to your project, your classes and gives hints to your collaborators on how to find quickly a specific
class and how you structured your logic. It also relies on patterns that have already been tested and are familiar to experienced
developers.

### Core - DB Changelog - API

A usual top level organization is split into 3 big packages that are:
* Core: This is the back bone of your application, your models, classes and services are basically stored into this package.
* DB Changelog: This is basically all the changes that are made to your database from LiquidBase or FlyWay.
* API: This is the interface between your application and the external world. Here is stored your HTTP logic and all your 
endpoints for consumers.

Each of those packages has its own child pom file that inherits from the parent one.
Of course, for smaller projects and very specific code parts, those specifications can be alleged.

### Domain name - EntityName - Configuration

Then comes your domain name, which is basically what is specified inside your pom, I will not enter deeper into this subject.

Finally comes the EntityName inside the core package, which normally includes your model, the service it is coupled with
and the Jpa repository interface.
