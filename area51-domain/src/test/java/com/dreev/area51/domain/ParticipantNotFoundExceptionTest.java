package com.dreev.area51.domain;

import org.junit.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class ParticipantNotFoundExceptionTest {

    @Test
    public void when_new_then_ok() {
        ParticipantNotFoundException actual = new ParticipantNotFoundException(UUID.randomUUID());
        assertThat(actual).isNotNull();
    }

}
