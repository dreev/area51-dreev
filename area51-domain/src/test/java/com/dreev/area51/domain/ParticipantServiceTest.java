package com.dreev.area51.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ParticipantServiceTest {

    private static final UUID PARTICIPANT_ID = UUID.randomUUID();
    private static final String PARTICIPANT_NAME = "aParticipant";

    @Mock
    private ParticipantMessageProducer participantMessageProducer;

    @Mock
    private ParticipantRepository participantRepository;

    @InjectMocks
    private ParticipantService participantService;

    private Participant participant;
    private ParticipantCreatedEvent participantCreatedEvent;

    @Before
    public void before() {
        participant = Participant.builder()
                .id(PARTICIPANT_ID)
                .name(PARTICIPANT_NAME)
                .build();

        participantCreatedEvent = ParticipantCreatedEvent.builder()
                .participant(participant)
                .build();

        when(participantRepository.findAll())
                .thenReturn(Collections.singletonList(participant));

        when(participantRepository.findById(eq(PARTICIPANT_ID)))
                .thenReturn(Optional.of(participant));

        when(participantRepository.save(any(Participant.class)))
                .thenReturn(participant);
    }

    @Test
    public void when_findAll_then_ok() {
        List<Participant> actual = participantService.findAll();
        assertThat(actual).hasSize(1);

        Participant participant = actual.get(0);
        assertThat(participant).isEqualTo(this.participant);
    }

    @Test
    public void given_participant_exists_when_getById_then_ok() {
        Participant actual = participantService.getById(PARTICIPANT_ID);
        assertThat(actual).isEqualTo(this.participant);
    }

    @Test(expected = ParticipantNotFoundException.class)
    public void given_participant_does_not_exist_when_getById_then_error() {
        participantService.getById(UUID.randomUUID());
    }

    @Test
    public void when_create_then_ok() {
        ParticipantCreationRequest creationRequest = ParticipantCreationRequest.builder()
                .name(PARTICIPANT_NAME)
                .build();
        Participant actual = participantService.create(creationRequest);

        assertThat(actual).isEqualTo(participant);

        verify(participantMessageProducer, times(1))
                .send(eq(participantCreatedEvent));
    }

}
