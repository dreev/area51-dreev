package com.dreev.area51.domain;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class InvalidParticipantCreationRequestExceptionTest {

    @Test
    public void when_new_then_ok() {
        InvalidParticipantCreationRequestException actual = new InvalidParticipantCreationRequestException("");
        assertThat(actual).isNotNull();
    }

}
