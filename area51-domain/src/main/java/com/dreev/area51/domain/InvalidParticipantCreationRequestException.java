package com.dreev.area51.domain;

import com.dreev.exception.business.BusinessException;

public final class InvalidParticipantCreationRequestException extends BusinessException {

    public InvalidParticipantCreationRequestException(String message) {
        super(message);
    }

}
