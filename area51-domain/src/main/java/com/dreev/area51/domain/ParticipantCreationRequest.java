package com.dreev.area51.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public final class ParticipantCreationRequest {

    private final String name;

}
