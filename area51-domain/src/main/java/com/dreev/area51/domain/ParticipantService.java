package com.dreev.area51.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class ParticipantService {

    private final ParticipantMessageProducer participantMessageProducer;
    private final ParticipantRepository participantRepository;

    public List<Participant> findAll() {
        return participantRepository.findAll();
    }

    public Participant getById(UUID participantId) {
        return participantRepository.findById(participantId)
                .orElseThrow(() -> new ParticipantNotFoundException(participantId));
    }

    @Transactional
    public Participant create(ParticipantCreationRequest creationRequest) {
        Participant participant = Participant.builder()
                .id(UUID.randomUUID())
                .name(creationRequest.getName())
                .build();
        Participant saved = participantRepository.save(participant);

        ParticipantCreatedEvent createdEvent = ParticipantCreatedEvent.builder()
                .participant(saved)
                .build();
        participantMessageProducer.send(createdEvent);

        return saved;
    }

}
