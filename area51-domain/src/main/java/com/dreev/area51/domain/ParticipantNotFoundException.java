package com.dreev.area51.domain;

import com.dreev.exception.business.BusinessException;

import java.util.UUID;

public final class ParticipantNotFoundException extends BusinessException {

    public ParticipantNotFoundException(UUID participantId) {
        super(participantId.toString());
    }

}
