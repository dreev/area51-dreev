package com.dreev.area51.domain;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ParticipantRepository {

    List<Participant> findAll();

    Optional<Participant> findById(UUID participantId);

    Participant save(Participant participant);

}
