package com.dreev.area51.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public final class ParticipantCreatedEvent {

    private final Participant participant;

}
