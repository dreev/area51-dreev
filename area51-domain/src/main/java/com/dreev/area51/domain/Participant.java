package com.dreev.area51.domain;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Builder
@Data
public final class Participant {

    private final UUID id;
    private final String name;

}
