package com.dreev.area51.domain;

public interface ParticipantMessageProducer {

    void send(ParticipantCreatedEvent event);

}
