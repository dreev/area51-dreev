package com.dreev.area51.http;

import com.dreev.area51.domain.ServicesConfiguration;
import com.dreev.area51.dto.DtoConfiguration;
import com.dreev.area51.messaging.rabbit.MessagingRabbitConfiguration;
import com.dreev.area51.persistence.jpa.PersistenceJpaConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({
        DtoConfiguration.class,
        MessagingRabbitConfiguration.class,
        PersistenceJpaConfiguration.class,
        ServicesConfiguration.class
})
public class Area51Application {

    public static void main(String[] args) {
        SpringApplication.run(Area51Application.class, args);
    }

}
