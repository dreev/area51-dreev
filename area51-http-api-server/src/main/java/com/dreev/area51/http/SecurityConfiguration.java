package com.dreev.area51.http;

import com.dreev.spring.boot.http.api.authentication.JwtCustomClaimAuthenticationConverter;
import com.dreev.spring.boot.http.api.security.HttpApiSecurityConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;

@Configuration
public class SecurityConfiguration extends HttpApiSecurityConfiguration {

    public SecurityConfiguration(
            SecurityProblemSupport problemSupport,
            JwtCustomClaimAuthenticationConverter jwtCustomClaimAuthenticationConverter) {
        super(problemSupport, jwtCustomClaimAuthenticationConverter);
    }

    @Override
    protected void doConfigure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .authorizeRequests()
                .mvcMatchers("/participants").hasAnyAuthority("GROUP_dreev_area51_admin", "SCOPE_dreev:area51:all")
                .anyRequest().authenticated();
    }

}
