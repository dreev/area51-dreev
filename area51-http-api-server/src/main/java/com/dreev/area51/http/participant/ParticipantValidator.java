package com.dreev.area51.http.participant;

import com.dreev.area51.client.dto.ParticipantCreationRequestDto;
import com.dreev.area51.domain.InvalidParticipantCreationRequestException;
import org.springframework.stereotype.Component;

@Component
public class ParticipantValidator {

    public void validate(ParticipantCreationRequestDto creationRequest) {
        if (null == creationRequest.getName()) {
            throw new InvalidParticipantCreationRequestException("name can not be null");
        }
    }

}
