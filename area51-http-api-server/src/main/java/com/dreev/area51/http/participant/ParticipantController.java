package com.dreev.area51.http.participant;

import com.dreev.area51.client.dto.ParticipantCreationRequestDto;
import com.dreev.area51.client.dto.ParticipantDto;
import com.dreev.area51.client.dto.ParticipantsDto;
import com.dreev.area51.domain.Participant;
import com.dreev.area51.domain.ParticipantCreationRequest;
import com.dreev.area51.domain.ParticipantService;
import com.dreev.area51.dto.ParticipantDtoMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.dreev.area51.client.http.Area51HttpApiPaths.*;

@RestController
@RequestMapping(PARTICIPANTS_PATH)
@RequiredArgsConstructor
@Slf4j
public class ParticipantController {

    private final ParticipantDtoMapper participantDtoMapper;
    private final ParticipantService participantService;
    private final ParticipantValidator participantValidator;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ParticipantDto create(
            @AuthenticationPrincipal Authentication auth,
            @RequestBody ParticipantCreationRequestDto participantCreationRequestDto) {
        log.info("Authenticated user : {}", auth);
        participantValidator.validate(participantCreationRequestDto);
        ParticipantCreationRequest creationRequest = participantDtoMapper.fromDto(participantCreationRequestDto);
        Participant participant = participantService.create(creationRequest);
        return participantDtoMapper.toDto(participant);
    }

    @GetMapping
    public ParticipantsDto findAll() {
        List<Participant> participants = participantService.findAll();
        return participantDtoMapper.toDto(participants);
    }

    @GetMapping(PARTICIPANT_ID_PATH)
    public ParticipantDto getById(@PathVariable(PARTICIPANT_ID) String participantIdDto) {
        UUID participantId = UUID.fromString(participantIdDto);
        Participant participant = participantService.getById(participantId);
        return participantDtoMapper.toDto(participant);
    }

}
