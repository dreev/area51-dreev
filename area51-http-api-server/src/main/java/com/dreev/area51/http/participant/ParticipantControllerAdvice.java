package com.dreev.area51.http.participant;

import com.dreev.area51.client.http.Area51HttpApiErrorType;
import com.dreev.area51.domain.InvalidParticipantCreationRequestException;
import com.dreev.area51.domain.ParticipantNotFoundException;
import com.dreev.area51.http.problem.ProblemMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;

@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class ParticipantControllerAdvice {

    private final ProblemMapper problemMapper;

    @ExceptionHandler(ParticipantNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public final Problem handle(ParticipantNotFoundException e) {
        log.error("a ParticipantNotFoundException error occurred", e);
        Area51HttpApiErrorType errorType = Area51HttpApiErrorType.PARTICIPANT_NOT_FOUND;
        return problemMapper.toProblem(errorType);
    }

    @ExceptionHandler(InvalidParticipantCreationRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final Problem handle(InvalidParticipantCreationRequestException e) {
        log.error("a InvalidParticipantCreationRequestException error occurred", e);
        Area51HttpApiErrorType errorType = Area51HttpApiErrorType.INVALID_PARTICIPANT_CREATION_REQUEST;
        return Problem.builder()
                .withType(errorType.getType())
                .withTitle(errorType.getTitle())
                .withStatus(Status.valueOf(errorType.getHttpStatusCode()))
                .withDetail(e.getMessage())
                .build();
    }

}
