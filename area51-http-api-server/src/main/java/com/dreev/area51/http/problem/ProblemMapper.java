package com.dreev.area51.http.problem;

import com.dreev.area51.client.http.Area51HttpApiErrorType;
import org.springframework.stereotype.Component;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;

@Component
public class ProblemMapper {

    public Problem toProblem(Area51HttpApiErrorType errorType) {
        return Problem.builder()
                .withType(errorType.getType())
                .withTitle(errorType.getTitle())
                .withStatus(Status.valueOf(errorType.getHttpStatusCode()))
                .withDetail(errorType.getDetail())
                .build();
    }

}
