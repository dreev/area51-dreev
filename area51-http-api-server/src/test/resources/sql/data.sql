-- clean participants
DELETE
FROM participant;

-- seed participants
INSERT INTO participant(id, name)
VALUES ('8beb26cc-12d7-499d-b106-e5fd1294058f', 'Keenu Reeves');
