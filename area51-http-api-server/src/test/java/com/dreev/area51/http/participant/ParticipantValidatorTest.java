package com.dreev.area51.http.participant;

import com.dreev.area51.client.dto.ParticipantCreationRequestDto;
import com.dreev.area51.domain.InvalidParticipantCreationRequestException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ParticipantValidatorTest {

    @InjectMocks
    private ParticipantValidator participantValidator;

    @Test
    public void given_a_valid_request_when_validate_then_ok() {
        ParticipantCreationRequestDto creationRequest = ParticipantCreationRequestDto.builder()
                .name("aParticipant")
                .build();

        participantValidator.validate(creationRequest);
    }

    @Test(expected = InvalidParticipantCreationRequestException.class)
    public void given_name_is_missing_when_validate_then_error() {
        ParticipantCreationRequestDto creationRequest = ParticipantCreationRequestDto.builder()
                .name(null)
                .build();

        participantValidator.validate(creationRequest);
    }


}
