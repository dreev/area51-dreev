package com.dreev.area51.http;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Binding.DestinationType;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * This Configuration class is in charge of declaring : Exchange, Queue and Binding.
 * This is a prerequisite that a system administrator would take care of on a production RabbitMQ broker.
 */
@Configuration
public class TestConfiguration {

    public static final String TEST_EXCHANGE = "area51_event_exchange";
    public static final String TEST_QUEUE = "participants.registered";

    @Bean
    public Exchange exchange() {
        return new TopicExchange(TEST_EXCHANGE);
    }

    @Bean
    public Queue queue() {
        return new Queue(TEST_QUEUE, false);
    }

    @Bean
    public Binding binding() {
        return new Binding(TEST_QUEUE, DestinationType.QUEUE, TEST_EXCHANGE, "#", Map.of());
    }

}
