package com.dreev.area51.http.test.data;

import lombok.Getter;

@Getter
public enum ParticipantTestData {

    KEENU_REEVES(
            "8beb26cc-12d7-499d-b106-e5fd1294058f",
            "Keenu Reeves"
    );

    private final String id;
    private final String name;

    ParticipantTestData(String id, String name) {
        this.id = id;
        this.name = name;
    }

}
