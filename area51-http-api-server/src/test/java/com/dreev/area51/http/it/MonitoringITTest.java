package com.dreev.area51.http.it;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static com.dreev.area51.client.http.Area51HttpApiPaths.PARTICIPANTS_PATH;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureWireMock(port = 0)
public class MonitoringITTest {

    @Value("http://localhost:${local.server.port}")
    private String url;

    @Before
    public void setUp() {
        baseURI = url;
    }

    @Test
    public void context_should_load() {
        // need an assertion so that Sonar does not complain about this test
        assertThat(true).isTrue();
    }

    @Test
    public void get_health_should_return_up() {
        given()
                .when()
                .get("/actuator/health")
                .then()
                .log().all()
                .statusCode(200)
                .body("status", is("UP"))
                .body("details", nullValue());
    }

    @Test
    public void get_health_should_return_more_info_when_logged() {
        given()
                .auth().preemptive().basic("admin", "not_password")
                .when()
                .get("/actuator/health")
                .then()
                .log().all()
                .statusCode(200)
                .body("status", equalTo("UP"))
                .body("details.diskSpace.status", equalTo("UP"));
        ;
    }

    @Test
    public void get_metrics_should_return_401_without_creds() {
        given()
                .when()
                .get("/actuator/metrics")
                .then()
                .log().all()
                .statusCode(401);
    }

    @Test
    public void get_metrics_should_return_stuff_with_credentials() {
        given()
                .auth().preemptive().basic("admin", "not_password")
                .when()
                .get("/actuator/metrics")
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void get_participants_should_return_unauthorized_with_actuator_credentials() {
        given()
                .auth().preemptive().basic("admin", "not_password")
                .when()
                .get(PARTICIPANTS_PATH)
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_UNAUTHORIZED);
    }

}
