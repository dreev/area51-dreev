package com.dreev.area51.http.it;

import com.dreev.area51.client.dto.ParticipantDto;
import com.dreev.area51.http.test.data.AccessToken;
import com.dreev.area51.http.test.data.ParticipantTestData;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static com.dreev.area51.client.http.Area51HttpApiPaths.*;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureWireMock(port = 0)
public class GetParticipantByIdITTest {

    @Value("http://localhost:${local.server.port}")
    private String url;

    @Before
    public void setUp() {
        baseURI = url;
    }

    @Test
    @Sql({"/sql/data.sql"})
    public void get_participant_by_id_should_work() {
        String participantUrl = PARTICIPANTS_PATH
                + PARTICIPANT_ID_PATH.replace(PARTICIPANT_ID_PLACEHOLDER, ParticipantTestData.KEENU_REEVES.getId());
        ParticipantDto participant = given()
                .auth().preemptive().oauth2(AccessToken.INSTANCE.getValue())
                .log().all()
                .when()
                .get(participantUrl)
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_OK)
                .extract().as(ParticipantDto.class);
        assertThat(participant.getId()).isEqualTo(ParticipantTestData.KEENU_REEVES.getId());
        assertThat(participant.getName()).isEqualTo(ParticipantTestData.KEENU_REEVES.getName());
    }

    @Test
    @Sql({"/sql/data.sql"})
    public void given_participant_does_not_exist_when_get_participant_by_id_then_not_found() {
        String participantUrl = PARTICIPANTS_PATH
                + PARTICIPANT_ID_PATH.replace(PARTICIPANT_ID_PLACEHOLDER, UUID.randomUUID().toString());
        given()
                .auth().preemptive().oauth2(AccessToken.INSTANCE.getValue())
                .log().all()
                .when()
                .get(participantUrl)
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

}
