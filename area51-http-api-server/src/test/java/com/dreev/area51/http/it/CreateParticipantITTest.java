package com.dreev.area51.http.it;

import com.dreev.area51.client.dto.ParticipantCreatedEventDto;
import com.dreev.area51.client.dto.ParticipantCreationRequestDto;
import com.dreev.area51.client.dto.ParticipantDto;
import com.dreev.area51.domain.Participant;
import com.dreev.area51.domain.ParticipantRepository;
import com.dreev.area51.http.TestConfiguration;
import com.dreev.area51.http.test.data.AccessToken;
import com.dreev.area51.http.test.data.ParticipantTestData;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.UUID;

import static com.dreev.area51.client.http.Area51HttpApiPaths.PARTICIPANTS_PATH;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureWireMock(port = 0)
public class CreateParticipantITTest {

    @Value("http://localhost:${local.server.port}")
    private String url;

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private AmqpTemplate amqp;

    private ParticipantCreationRequestDto creationRequest;

    @Before
    public void setUp() {
        baseURI = url;

        creationRequest = ParticipantCreationRequestDto.builder()
                .name(ParticipantTestData.KEENU_REEVES.getName())
                .build();
    }

    @Test
    public void post_participant_should_persist_participant() {
        ParticipantDto created = given()
                .contentType(ContentType.JSON)
                .auth().preemptive().oauth2(AccessToken.INSTANCE.getValue())
                .body(creationRequest)
                .log().all()
                .when()
                .post(PARTICIPANTS_PATH)
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_CREATED)
                .extract().as(ParticipantDto.class);

        Optional<Participant> foundInDb = participantRepository.findById(UUID.fromString(created.getId()));
        assertThat(foundInDb).isPresent();
        assertThat(foundInDb.get().getName()).isEqualTo(ParticipantTestData.KEENU_REEVES.getName());

        checkMessageSent();
    }

    @Test
    public void post_participant_without_name_should_return_error() {
        ParticipantCreationRequestDto creationRequest = ParticipantCreationRequestDto.builder()
                .build();
        given()
                .contentType(ContentType.JSON)
                .auth().preemptive().oauth2(AccessToken.INSTANCE.getValue())
                .body(creationRequest)
                .log().all()
                .when()
                .post(PARTICIPANTS_PATH)
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void post_participant_without_token_should_return_401() {
        given()
                .contentType(ContentType.JSON)
                .body(creationRequest)
                .log().all()
                .when()
                .post(PARTICIPANTS_PATH)
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_UNAUTHORIZED);
    }

    /**
     * Check that we received a message within 3 seconds in the
     * emergency_charge_command_monitoring queue.
     */
    private void checkMessageSent() {
        ParticipantCreatedEventDto actual = (ParticipantCreatedEventDto) amqp.receiveAndConvert(TestConfiguration.TEST_QUEUE, 3_000);
        assertThat(actual).isNotNull();

        ParticipantDto participant = actual.getParticipant();
        assertThat(participant).isNotNull();
        assertThat(participant.getId()).isNotNull();
        assertThat(participant.getName()).isEqualTo(ParticipantTestData.KEENU_REEVES.getName());
    }

}
