package com.dreev.area51.http.problem;

import com.dreev.area51.client.http.Area51HttpApiErrorType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.zalando.problem.Problem;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ProblemMapperTest {

    @InjectMocks
    private ProblemMapper problemMapper;

    @Test
    public void when_toProblem_then_ok() {
        Area51HttpApiErrorType errorType = Area51HttpApiErrorType.PARTICIPANT_NOT_FOUND;
        Problem actual = problemMapper.toProblem(errorType);
        assertThat(actual.getType()).isEqualTo(errorType.getType());
        assertThat(actual.getTitle()).isEqualTo(errorType.getTitle());
        assertThat(actual.getStatus()).isNotNull();
        assertThat(actual.getStatus().getStatusCode()).isEqualTo(errorType.getHttpStatusCode());
        assertThat(actual.getDetail()).isEqualTo(errorType.getDetail());
    }

}
